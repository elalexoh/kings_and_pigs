extends Node
class_name State

var state_machine: Node
var character: KinematicBody2D
var anim_character: AnimationPlayer

var speed: float
var velocity: Vector2
var direction: Vector2

func enter():
	pass

func physics(_delta):
	pass

func handle_input(_event):
	pass
	
func get_direction():
	if character.is_in_group("Players"):
		
		direction.x = 0
		
		if Input.is_action_pressed("right"):
			direction.x = 1
		if Input.is_action_pressed("left"):
			direction.x = -1
	
	if direction.x != 0:
		character.get_node('Pivot').scale.x = direction.x

func exit():
	pass

func move():
	velocity.x = speed * direction.x
	character.velocity.x = velocity.x
