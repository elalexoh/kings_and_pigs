extends Node2D

func _ready():
	pos_player()
	configure_camera()

func transition(next_scene):
	get_tree().change_scene(next_scene)

func configure_camera():
	var map = get_node("Tilemaps/TileMap")

#	?? limit left from first tile
	get_node("King/Camera2D").limit_left = map.get_used_cells()[1].x * map.cell_size.x
#	?? limit right from last tile
	get_node("King/Camera2D").limit_right = map.get_used_cells()[map.get_used_cells().size() - 1].x * map.cell_size.x

#	?? limit top from first tile
	get_node("King/Camera2D").limit_top = map.get_used_cells()[1].y * map.cell_size.y
#	?? limit bottom from last tile
	get_node("King/Camera2D").limit_bottom = map.get_used_cells()[map.get_used_cells().size() - 1].y * map.cell_size.y
	
func pos_player():
	for door in get_node("Doors").get_children():
		if door.get_position_in_parent() == 0 :
			$King.global_position = door.get_node("start_position").global_position
			door.anim_character.play_backwards('Opening')
		
