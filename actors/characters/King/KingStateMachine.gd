extends StateMachine

func _ready():
	states = {
		"Idle": get_node("Idle"),
		"Move": get_node("Move"),
		"Jump": get_node("Jump"),
	}
	current_state = states["Idle"]
	active = true
	change_state("Idle")
