extends State

func enter():
	anim_character.play('Jump')
	jump()

func jump():
	character.velocity.y = -190
	
func physics(_delta):
	.get_direction()
	.move()
	
	if character.is_on_floor():
		state_machine.change_state('Idle')
