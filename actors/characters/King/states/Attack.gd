extends State

func enter():
	anim_character.play('Attack')
	anim_character.connect("animation_finished", self, "attacked")
	attack()

func attack():
	if character.enemies.size() > 0:
		for enemy in character.enemies:
			if not enemy.state_machine.current_state.name in ["Dead", "Hit"]:
				enemy.state_machine.change_state("Hit")
				
func attacked(anim_name: String):
	if anim_name == "Attack":
		state_machine.change_state("Idle")

func exit():
	anim_character.disconnect("animation_finished", self, "attacked")

func physics(_delta):
	get_direction()
	move()
