extends State


func enter():
	anim_character.play('Dead')
	
func physics(delta):
	if character.is_on_floor():
		state_machine.active = false
		character.set_physics_process(false)
