extends State


func enter():
	anim_character.play("Idle")

func physics(_delta):
	.get_direction()
	.move()
	
	if direction.x != 0:
		state_machine.change_state("Move")
