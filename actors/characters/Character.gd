extends KinematicBody2D

var direction: Vector2 = Vector2(0,5)
var velocity: Vector2 = Vector2.ZERO
export var debug_mode: bool = false

export var speed: float
export var gravity: float

export var lifes: int

var enemies = []

onready var state_machine = get_node("KingStateMachine")

func _ready():
	if not(debug_mode):
		get_node("Debug").hide()

	state_machine.character = self

func _physics_process(delta):
	#delta: el tiempo que toma en llamar la funcion en 60 frames
	velocity.y += gravity * delta
	
	if debug_mode:
		get_node("Debug/X").text = str(int(round(velocity.x)))
		get_node("Debug/Y").text = "%s" % velocity.y
	
	move_and_slide(velocity, Vector2(0,-1))
	
	if is_on_floor():
		velocity.y = 0
 
func _on_detect_enemy_body_entered(body):
	if body and body.is_in_group("Enemies"):
		enemies.append(body)
		
func _on_detect_enemy_body_exited(body):
	if body and body.is_in_group("Enemies"):
		enemies.erase(body)
