extends State

func enter():
	anim_character.play("Hit")
	anim_character.connect("animation_finished", self, "hited")
	
	jump()

func hited(anim_name):
	if anim_name == "Hit":
		character.lifes -= 1
		
		if character.lifes <= 0:
			state_machine.change_state("Dead")
			return
		
		state_machine.change_state('Idle')

func exit():
	anim_character.disconnect("animation_finished", self, "hited")

func jump():
	character.velocity.y = -90
	character.velocity.x = speed * character.direction.x
	
