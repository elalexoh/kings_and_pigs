extends State


func enter():
	anim_character.play("Move")
	
func handle_input(event):
	if Input.is_action_just_pressed('jump'):
		state_machine.change_state('Jump')

func physics(delta):
	.get_direction()
	.move()
	
	if direction.x == 0:
		state_machine.change_state("Idle")
