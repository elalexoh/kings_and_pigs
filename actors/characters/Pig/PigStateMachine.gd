extends StateMachine

func _ready():
	states = {
		"Idle"		: get_node("Idle"),
		"Move"		: get_node("Move"),
		"Jump"		: get_node("Jump"),
		"Attack"	: get_node("Attack"),
		"Hit"		: get_node("Hit"),
		"Dead"		: get_node("Dead"),
	}
	current_state = states["Idle"]
	active = true
	change_state("Idle")
