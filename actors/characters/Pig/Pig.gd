extends KinematicBody2D

var direction: Vector2 = Vector2(0,5)
var velocity: Vector2 = Vector2.ZERO

export var speed: float
export var gravity: float

export var lifes: int

onready var detect_left_map = get_node("left_map")
onready var detect_right_map = get_node("right_map")
onready var detect_left_player = get_node("right_player")
onready var detect_right_player = get_node("left_player")

var temp_jumps_right: int
var temp_jumps_left: int

onready var state_machine = get_node("PigStateMachine")

func _ready():
	state_machine.character = self

func _physics_process(delta):
	#delta: el tiempo que toma en llamar la funcion en 60 frames
	velocity.y += gravity * delta
	
	
	
	move_and_slide(velocity, Vector2(0,-1))
	
	if is_on_floor():
		velocity.y = 0
		
	var detect_col = detect_collision()
	
	if not detect_col == null and detect_col.is_in_group('Players') and detect_col.global_position.distance_to(global_position) < 30:
		if not detect_col.state_machine.current_state.name in ["Dead", "Hit"] and not state_machine.current_state.name == "Attack":
			state_machine.change_state("Attack")
			detect_col.state_machine.change_state("Hit")
	
func detect_collision():
	var player
	var friend
	
	if get_node("time_move").is_stopped():
		direction.x = 0
		
	if detect_right_player.is_colliding():
		if detect_right_player.get_collider().is_in_group("Players"):
			player = detect_right_player.get_collider()
			direction.x = 1
			get_node("time_move").start()
			return player
		elif detect_right_player.get_collider().is_in_group("Enemies"):
			friend= detect_right_player.get_collider()
			if friend.global_position.distance_to(global_position) < 30:
				direction.x = -1
				get_node("time_move").start()

	if detect_left_player.is_colliding():
		if detect_left_player.get_collider().is_in_group("Players"):
			player = detect_left_player.get_collider()
			direction.x = -1
			get_node("time_move").start()
			return player
		elif detect_left_player.get_collider().is_in_group("Enemies"):
			friend= detect_left_player.get_collider()
			if friend.global_position.distance_to(global_position) < 30:
				direction.x = 1
				get_node("time_move").start()

	if detect_right_map.is_colliding():
		if detect_right_map.get_collider() is TileMap:
			if not get_node("time_move").is_stopped():
				if velocity.y == 0:
					if temp_jumps_right == 2:
						direction.x = -1
						get_node("time_move").start()
						return
					state_machine.change_state("Jump")
					temp_jumps_right += 1
					get_node("time_move").start()
	else:
		temp_jumps_right = 0

	if detect_left_map.is_colliding():
		if detect_left_map.get_collider() is TileMap:
			if not get_node("time_move").is_stopped():
				if velocity.y == 0:
					if temp_jumps_left == 2:
						direction.x = 1
						get_node("time_move").start()
						return
					state_machine.change_state("Jump")
					temp_jumps_left += 1
					get_node("time_move").start()
	else:
		temp_jumps_left = 0
		
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
