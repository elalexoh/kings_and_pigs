extends Area2D

export var next_level: String

var anim_character: AnimationPlayer
var crossing: bool = false 

func _ready():
	set_process_input(false)
	anim_character = get_node("AnimationPlayer" )

func _input(_event):
	if Input.is_action_just_pressed("ui_accept"):
		owner.transition(next_level)
		anim_character.play('Opening')
		crossing = true
		set_process_input(false)

func _on_Door_body_entered(body):
	if body and body.is_in_group('Players'):
		set_process_input(true)


func _on_Door_body_exited(body):
	if body and body.is_in_group('Players'):
		set_process_input(false)
